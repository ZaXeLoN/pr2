#include <iostream>
#include <string.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include "functions.h"

bool writeNewItem(itemBD newItem,const char *FName) {
	FILE* fAdd = fopen(FName,"ab");
	if (fAdd == NULL) return true;
	if (fwrite(&newItem,sizeof(itemBD),1,fAdd) ==0) return true;
	fclose(fAdd);
	return false;
}

bool delItem(int numbDel,const char *FName) {
	itemBD readItem;
	FILE* fRm = fopen(FName,"rb+");
	if (fRm == NULL) return true;
	if (fseek(fRm, (numbDel) * sizeof(itemBD), SEEK_SET)==0) {
		while (fread(&readItem, sizeof(itemBD), 1, fRm)){
			fseek(fRm, (numbDel-1) * sizeof(itemBD), SEEK_SET);
			fwrite(&readItem, sizeof(itemBD), 1, fRm);
			numbDel++;
			fseek(fRm, (numbDel)*sizeof(itemBD), SEEK_SET);
		}
		truncate(FName,(numbDel-1)*sizeof(itemBD));
	}
	return false;
}

bool writeChangeItem(itemBD changeItem,int numbChange,const char *FName) {
	FILE* fChange = fopen(FName,"rb+");
	if (fChange == NULL) return true;
	fseek(fChange, (numbChange-1) * sizeof(itemBD), SEEK_SET);
	fwrite(&changeItem, sizeof(itemBD), 1, fChange);
	fclose(fChange);
	return false;
}