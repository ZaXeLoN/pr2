#include <iostream>
#include <string.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include "functions.h"

const char *FName="./save.dat";

using namespace std;

void showItem(bool block) {
	system("clear");
	itemBD readItem;
	cout << "Записи в базе данных:" << endl << endl; 
	FILE* fShow = fopen(FName,"rb");
	if (fShow == NULL) {
		cout << "Ошибка открытия файла" << endl;
		block = true;
	} else {
		int numbItem = 1;
		while (fread(&readItem, sizeof(itemBD), 1, fShow)) {
			coutItemBD(readItem,numbItem);
			numbItem++;
		}
		if (false) {
			cout << "Ошибка чтения из файла" << endl;
			block = true;
		} else if (numbItem == 1) {
			cout << "В базе данных нет записей" << endl;
			block = true;
		}
		fclose(fShow);
	}
	if (block) blockWork();
}

void addItem() {
	system("clear");
	itemBD newItem = cinInfo(false);
	if (writeNewItem(newItem,FName)) {
		cout << "Во время записи произошла ошибка!" << endl;
	}
	blockWork();
}

void rmItem() {
	showItem(false);
	cout << "Введите номер записи которую хоите удалить(0 - для отмены):" << endl;
	int numbDel = cinInteger();
	if (numbDel==0) return;
	FILE* fRm = fopen(FName,"rb+");
	if (fRm == NULL) {
		cout << "Ошибка открытия файла" << endl;
	} else {
		fseek(fRm, 0, SEEK_END);
		if((sizeof(itemBD) * numbDel)>ftell(fRm)) {
			cout << "Записи с таким номером нет!" << endl;
		} else {
			if (delItem(numbDel,FName)) {
				cout << "Не получилось удалить запись" << endl;
			}
		}
		fclose(fRm);
	}
	blockWork();
}

void changeItem() {
	showItem(false);
	cout << "Введите номер записи которую хоите изменить(0 - для отмены):" << endl;
	int numbChange = cinInteger();
	if (numbChange==0) return;
	cout << endl;
	FILE* fChange = fopen(FName,"rb+");
	if (fChange == NULL) {
		cout << "Ошибка открытия файла" << endl;
	} else {
		fseek(fChange, 0, SEEK_END);
		if((sizeof(itemBD) * numbChange)>ftell(fChange)) {
			cout << "Записи с таким номером нет!" << endl;
		} else {
			if(writeChangeItem(cinInfo(false),numbChange,FName)){
				cout << "Не получилось изменить запись" << endl;
			}
		}
		fclose(fChange);
	}
	blockWork();
}