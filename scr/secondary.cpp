#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <iomanip>
#include "functions.h"

using namespace std;

int cinInteger() {
	int tempCin = 0;
	while (!(cin >> tempCin)) {
		cin.clear();
		cin.ignore(100000, '\n');
		cout << "Вы ввели не цифру!!!" << endl;
		cout << "Введите цифру:" << endl;
	}
	cin.clear();
	cin.ignore(100000, '\n');
	return tempCin;
}

void blockWork() {
	string temp;
	cout << "Для того что бы продолжить введите: \"Y\"" << endl;
	do {
		cin >> temp;
	} while ((temp != "Y")&&(temp != "y"));
}

void coutItemBD(itemBD temp,int numbItem) {
	if (numbItem != -1) { 
		cout << "Номер: " << numbItem << endl;
	}
	cout << "Название больницы: " << temp.name << endl;
	cout << "Адрес больницы: " << temp.address << endl;
	cout << "Дата открытия: " << temp.openingDate.day << "." << temp.openingDate.month
	<< "." << temp.openingDate.year << endl;
	cout << "Количество кроватей: " << temp.countBed << endl;
	cout << endl;
}

itemBD cinInfo(bool skip) {
	itemBD newItem;
	if (!skip) {
		cout << "Введите название больницы:" << endl;
		cin >> newItem.name;
		cout << "Введите адрес больницы:" << endl;
		cin >> newItem.address;
		newItem.openingDate = cinDate();
		cout << "Введите количество кроватей:" << endl;
		while ((newItem.countBed = cinInteger()) <= 0) {
			cout << "Кроватей не может быть меньше одной!" << endl;
			cout << "Введите количество кроватей:" << endl;
		}
	} else {
		newItem.countBed = 0;
	}
	return newItem;
}

dateRec cinDate() {
	dateRec temp;
	int yr,mt,day;
	bool goodCin;
	do {
		goodCin = true;
		cout << "Введите дату открытия(в формате \"23.01.1998\"):" << endl;
		cin>>setw(2)>>day;
		if (day < 1 || day > 31) goodCin = false;
		cin.ignore();
		if (goodCin) cin>>setw(2)>>mt;
		if (mt < 1 || mt > 12) goodCin = false;
		cin.ignore();
		if (goodCin) cin>>setw(4)>>yr;
		if (yr < 1900 || mt > 2100) goodCin = false;
		if (cin.fail()||(!goodCin)) {
			cout << "Дата введена не верно!" << endl;
			cin.clear();
		}
		cin.ignore(100000, '\n');
	} while (cin.fail()||(!goodCin));
	temp.day = day;
	temp.month = mt;
	temp.year = yr;
	return temp;
}