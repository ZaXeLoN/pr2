#define BOOST_TEST_MODULE example
#include <boost/test/included/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>
const char *fileName="./save.dat";
#include "functions.h"

using boost::test_tools::output_test_stream;

BOOST_AUTO_TEST_SUITE(testWriteNewItem) 

BOOST_AUTO_TEST_CASE(testWriteNewItem)
{
	itemBD x;
	BOOST_CHECK(!(writeNewItem(x,fileName)));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(testDelItem) 

BOOST_AUTO_TEST_CASE(testDelItem)
{
	BOOST_CHECK(!delItem(1,fileName));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(testChaneItem) 

BOOST_AUTO_TEST_CASE(testChaneItem)
{
	itemBD x;
	BOOST_CHECK(!writeChangeItem(x,1,fileName));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(testCinCinDay) 

BOOST_AUTO_TEST_CASE(testCinCinDay)
{
	dateRec test = cinDate();
	BOOST_CHECK(test.day>0&&test.day<32);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(testCinCinMonth) 

BOOST_AUTO_TEST_CASE(testCinCinDay)
{
	dateRec test = cinDate();
	BOOST_CHECK(test.month>0&&test.month<13);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(testCinCinYear) 

BOOST_AUTO_TEST_CASE(testCinCinDay)
{
	dateRec test = cinDate();
	BOOST_CHECK(test.year>1900&&test.year<2100);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(testCinInfo) 

BOOST_AUTO_TEST_CASE(ttestCinInfo)
{
	itemBD x = cinInfo(true);
	BOOST_CHECK(x.countBed==0);
}

BOOST_AUTO_TEST_SUITE_END()